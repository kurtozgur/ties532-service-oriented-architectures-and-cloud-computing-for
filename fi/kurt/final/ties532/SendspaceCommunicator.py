import threading
try:
    from io import BytesIO, StringIO
    from urllib.parse import urlencode
    from urllib.request import urlopen, Request
except ImportError:
    from StringIO import StringIO as BytesIO
    from urllib import urlencode
    from urllib2 import urlopen, Request
import xml.etree.ElementTree as etree

# This class is a wrapper for sendspace method calls and parsing their returns 
class SendspaceCommunicator:

	def __init__( self, api_url ):
		self.api_url = api_url
		# get a common thread lock from threading module
		self.c_lock = threading.Lock()

	# utility method to call methods from sendspace api
	def call_ss_method( self, method, params=[] ):

		""" 
			This class is instantiated once in Sendspace.py, hence the same object will be used during multiple file uploads
			If this method is called by two processes at once, the result will be unreliable
			Use of thread lock prevents this method to be called simultaneously
		"""
		# acquire the common thread lock to provide synchronisation
		self.c_lock.acquire()

		url = self.api_url+"?method="+method
		url += "&" + urlencode( params )
		response = urlopen( url )
		content = response.read().decode( 'utf-8' )
		
		parsed = self.parse_xml_content( content )


		if not parsed or len( parsed.items() ) == 0:
			parsed = None
		elif parsed[ "result" ][ "method" ] != method:
			# release the lock
			self.c_lock.release()
			raise RuntimeError( "sendspace operation failed: Requested and retrieved methods are different" ) 
		
		# release the lock
		self.c_lock.release()
		
		return parsed


	def parse_xml_content( self, xml ):

	    root = etree.fromstring( xml )
	    ret = {
	        "params": {},
	    }

	    if root.tag != "result":
	        return None

	    ret[ "result" ] = root.attrib

	    for child in root:
	        tag_val = {
	            "value": child.text,
	            "attributes": child.attrib,
	        }
	        try:
	            ret[ "params" ][ child.tag ].append( tag_val )
	        except KeyError:
	            ret[ "params" ][ child.tag ] = [ tag_val ]

	    if len(ret.items()) == 0:
	        return None

	    return ret