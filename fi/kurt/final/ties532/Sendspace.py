from flask import redirect
import io
import pycurl
import threading
from fi.kurt.final.ties532.RepoFile import *
from fi.kurt.final.ties532.SendspaceCommunicator import *
from os.path import getsize, realpath

class Sendspace:

	def __init__( self, session_key, local_path ):
		self.session_key = session_key
		self.local_path = local_path
		self.ss_com = SendspaceCommunicator( "http://api.sendspace.com/rest/" )
		self.c_lock = threading.Lock()
		io.DEFAULT_BUFFER_SIZE = 0 # denotes the buffer size while uploading a file

	# get file structure from sendspace repo
	# ex: http://api.sendspace.com/rest/?method=folders.getcontents&session_key=key&folder_id=0
	def get_files_from_repo( self, folder_path = "", folder_id = 0 ):

		folders = []
		params = [ ( "session_key", self.session_key ), ( "folder_id", folder_id ) ]
		content  = self.ss_com.call_ss_method( "folders.getcontents", params )

		if content[ "result" ][ "status" ] != "ok":
			raise RuntimeError( "Can not retrieve files from sendspace. Login might have expired on sendspace." )

		for k,v in content[ "params" ].items():
			# add files to the list
			if k == "file":
				for fl in v:
					folders.append( RepoFile( False, (folder_path+"/"+fl[ "attributes" ][ "name" ] ) ) )

			elif k == "folder": # recusively get files and sub folders
				for fl in v:
					folders.append( RepoFile( True, (folder_path+"/"+fl[ "attributes" ][ "name" ] ), 
													file_id = fl[ "attributes" ][ "id" ], 
													parent_id = fl[ "attributes" ][ "parent_folder_id" ] ) )
					folders = folders + self.get_files_from_repo( ( folder_path+"/"+fl[ "attributes" ][ "name" ] ), fl[ "attributes" ][ "id" ] )

			else:
				# do nothing
				pass

		return folders


	# Creates the given folder on sendspace file system
	def create_folder( self, folder, parent_folder_id, parent_folder_path ):

		params = [ ( "session_key", self.session_key ), ( "name", folder ), ( "parent_folder_id", parent_folder_id ) ]
		content  = self.ss_com.call_ss_method( "folders.create", params )
		#print( str, content )
		if content[ "result" ][ "status" ] == "ok":
			return RepoFile( True, (parent_folder_path+"/"+content[ "params" ][ "folder" ][ 0 ][ "attributes" ][ "name" ] ), 
										file_id = content[ "params" ][ "folder" ][ 0 ][ "attributes" ][ "id" ], 
										parent_id = content[ "params" ][ "folder" ][ 0 ][ "attributes" ][ "parent_folder_id" ] ) 
		else:
			raise RuntimeError( "Folder not created!" ) 


	# upload given file to the sendspace repo
	def upload_file( self, upload_file ):
		
		# acquire the common thread lock to provide synchronisation
		self.c_lock.acquire()

		# receive file upload information from sendspace	
		params = [ 
					( "session_key", self.session_key ), 
				   	( "speed_limit", 0 ), 
				   	( "folder_id", upload_file.get_parent_id() ) 
				]
		content  = self.ss_com.call_ss_method( "upload.getinfo", params )				

		if content: 

			try:

				# upload information from sendspace
				post_url = content[ "params" ][ "upload" ][ 0 ][ "attributes" ][ "url" ]
				identifier = content[ "params" ][ "upload" ][ 0 ][ "attributes" ][ "upload_identifier" ]
				extra_info = content[ "params" ][ "upload" ][ 0 ][ "attributes" ][ "extra_info" ]
				max_filesize = int( content[ "params" ][ "upload" ][ 0 ][ "attributes" ][ "max_file_size" ] )

				filename = realpath( self.local_path+upload_file.get_filename() )

				if getsize( filename ) > max_filesize:
					raise RuntimeError( "File is larger than the size sendspace allows uploading: %s" %( upload_file.get_path() ) )

				c = pycurl.Curl()

				# arguments required by sendspace 
				args = [
					( "MAX_FILE_SIZE", str( max_filesize ) ),
					( "UPLOAD_IDENTIFIER", identifier ),
					( "extra_info", extra_info ),
					( "folder_id", upload_file.get_parent_id() ),
					( "userfile", ( pycurl.FORM_FILE, filename, ) ),
					]

				c.setopt( c.URL, post_url )
				c.setopt( c.HTTPPOST, args )

				b = BytesIO()
				c.setopt( pycurl.WRITEDATA, b )
				c.perform()
				c.close()

				# check return content
				content = b.getvalue().decode( 'utf-8' )
				if "upload_status=fail" in content:
					raise RuntimeError( "Upload failed for file: %s - %s" % ( upload_file.get_path(), content ) )

				for line in content.splitlines():
					if "file_id=" in line:
						file_id = line.split( "=" )[ 1 ]
						break

				if not file_id:
					raise RuntimeError( "Upload failed for file: %s - File id could not found on sendspace" % ( upload_file.get_path() ) )

				return True

			except Exception as err:				
				raise RuntimeError( str( err ) )
				return False
			finally:
				# release the lock
				self.c_lock.release()	

		else:
			# release the lock
			self.c_lock.release()
			raise RuntimeError( "Upload information could not be retrieved from sendspace!" )
			return False

		
