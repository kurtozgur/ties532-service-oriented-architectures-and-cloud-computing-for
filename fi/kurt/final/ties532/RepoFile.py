
class RepoFile( object ):

	def __init__( self, is_dir, path, file_id = None, parent_id = None ):
		self.is_dir = is_dir
		self.path = path # dropbox path
		self.file_id = str( file_id )
		self.parent_id = str( parent_id )
		#self.subfiles = []
		# dropbox file name (without the folders)
		ta = path.split( "/" )
		self.filename = ta[ len( ta ) - 1 ] 

	def get_is_dir( self ):
		return self.is_dir

	def get_path( self ):
		return self.path

	def get_filename( self ):
		return self.filename

	def get_subfiles( self ):
		return self.subfiles

	def get_file_id( self ):
		return self.file_id

	def get_parent_id( self ):
		return self.parent_id

	def set_parent_id( self, parent_id ):
		self.parent_id = parent_id

	# to assert equality when same path and file type are equal
	def __eq__( self, other ):
		# Override the default Equals behavior
		if isinstance( other, self.__class__ ):
			return self.path == other.path and self.is_dir == other.is_dir
		return NotImplemented

	def __ne__( self, other ):
		# Define a non-equality test
		if isinstance( other, self.__class__ ):
			return not self.__eq__(other)
		return NotImplemented

	def __hash__( self ):
		# Override the default hash behavior (that returns the id or the object)
		return hash( tuple( sorted( self.__dict__.items() ) ) )
