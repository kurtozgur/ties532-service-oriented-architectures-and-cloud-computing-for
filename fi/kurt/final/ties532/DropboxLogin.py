from dropbox.client import DropboxOAuth2Flow

# Class for handling dropbox log-in process
class DropboxLogin:

    def __init__( self, app_key, app_secret ):
        self.app_key = app_key
        self.app_secret = app_secret

    # Start dropbox auth process
    def dropbox_auth_start( self, web_app_session, request, redirect_uri ):
        return self.get_dropbox_auth_flow( web_app_session, redirect_uri ).start()

    def get_dropbox_auth_flow( self, web_app_session, redirect_uri ):
        return DropboxOAuth2Flow( self.app_key, self.app_secret, redirect_uri,
                                    web_app_session, "dropbox-auth-csrf-token" )

    # Finish dropbox auth process
    # return tuple -> return link / return message
    def dropbox_auth_finish( self, web_app_session, request, redirect_uri ):

        r_link = "/"
        r_msg = None
        try:
            access_token, user_id, url_state = \
                    self.get_dropbox_auth_flow( web_app_session, redirect_uri ).finish( request.args )
            web_app_session[ "dbtoken" ] = access_token
            r_link = "/"
        except DropboxOAuth2Flow.BadRequestException as e:
            http_status(400)
        except DropboxOAuth2Flow.BadStateException as e:
            r_link = "/"
        except DropboxOAuth2Flow.CsrfException as e:
            http_status(403)
        except DropboxOAuth2Flow.NotApprovedException as e:
            r_msg = "Not approved!"
            r_link = "/"
        except DropboxOAuth2Flow.ProviderException as e:        
            http_status(403)
            r_link = "/"
        return r_link, r_msg

    #log-out dropbox session, simply remove dropbox token in session
    def dropbox_logout( self, session ):
        if "dbtoken" in session:
            del session[ "dbtoken" ]
            return True
        else:
            return False
