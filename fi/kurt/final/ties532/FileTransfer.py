import threading
import os

class FileTransfer( threading.Thread ):

	def __init__( self, thread_id, repo_file, db_obj, ss_obj, local_path ):
		threading.Thread.__init__( self )
		self.thread_id = thread_id
		self.file = repo_file
		self.db_obj = db_obj
		self.ss_obj = ss_obj
		self.completed = False
		self.local_path = local_path
		self.message = ""

	def run( self ):

		try:

			if self.db_obj.download_file( self.file ):

				if self.ss_obj.upload_file( self.file ):
					self.completed = True
				else: 
					self.completed = False
				# delete file from local filesystem
				os.remove( self.local_path+self.file.get_filename() )

			else:
				self.completed = False

		except Exception as re:
			self.message = str( re )


	# getters
	def is_completed( self ):
		return self.completed

	def get_message( self ):
		return self.message

	def get_file( self ):
		return self.file
