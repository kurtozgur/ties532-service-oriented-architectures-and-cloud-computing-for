from dropbox.client import DropboxClient
from dropbox.rest import ErrorResponse 
from fi.kurt.final.ties532.RepoFile import *
import uuid
import threading

class Dropbox:

	def __init__( self, token, local_path ):
		self.token = token
		self.client = DropboxClient( token )
		self.local_path = local_path
		self.c_lock = threading.Lock()

	def get_files_from_repo( self ):

		try:
			# start crawling from root folder
			root = self.get_metadata( "/" )
		except ErrorResponse as e:
			return str( e )
		return root

	# Recursively crawls through file subsystem and returns an array of RepoFile objects
	# (Obsolete)using uuid4 to give dropbox files a unique name to save in local temp folder
	def get_metadata( self, folder ):

		rl = [] #return array

		root = self.client.metadata( folder )
		if root:
			if len( root[ "contents" ] ) > 0:
				# crawl
				for fd in root[ "contents" ]:
					#print( fd[ "path" ]," ", fd[ "is_dir" ] )
					#rl.append( RepoFile( fd[ "is_dir" ], fd[ "path" ], file_id = str( uuid.uuid4() ) ) )
					rl.append( RepoFile( fd[ "is_dir" ], fd[ "path" ] ) )
					if fd[ "is_dir" ]:
						try:
							rl = rl + self.get_metadata( fd[ "path" ] )
						except TypeError:
							pass

		return rl

	# download file from dropbox
	def download_file( self, file ):

		try:

			# acquire the common thread lock to provide synchronisation
			self.c_lock.acquire()
			
			out = open( self.local_path+file.get_filename(), "wb" )
			with self.client.get_file( file.get_path() ) as f:
				out.write( f.read() )
			
			return True

		except RuntimeError as e:
			self.message = str( e )
			return False
		finally:
			# release the lock
			self.c_lock.release()


	# return dropbox client
	def get_db_client( self ):
		return self.client
