import re #regular expression
from fi.kurt.final.ties532.Dropbox import *
from fi.kurt.final.ties532.Sendspace import *
from fi.kurt.final.ties532.FileTransfer import *
from fi.kurt.final.ties532.RepoFile import *

class SynchController:

	# Initialise a controller object with the given tokens
	def __init__( self, db_token, ss_key ):

		# may read local save folder from config file
		self.local_path = "E:/"	

		self.db_obj = Dropbox( db_token, self.local_path )
		self.ss_obj = Sendspace( ss_key, self.local_path )
		self.dir_difference = []
		self.file_difference = []
		self.ss_current_folders = []# needed for ids
		self.error_list = []

	def get_error_list( self ):
		# get only unique values
		eset = set( self.error_list )
		return list( eset )

	# Starts the synch operation from Dropbox to Sendspace
	def start_synchronisation( self ):

		if not self.db_obj or not self.ss_obj:
			return False
		else: 
			try:

				# find the differences between repositories
				self.repo_difference()
				same = True

				if len( self.dir_difference ) > 0:
					same = False
					for f in self.dir_difference:
						self.create_folder_on_sendspace( f.get_path() )
					

				if len( self.file_difference ) > 0:
					same = False
					self.transfer_files()

				if same:
					self.error_list.append( "All files are available in the sendspace repository" )

				return True

			except Exception as e:
				raise RuntimeError( "Synchronisation process failed with the following error: " + str( e ) )

	# Find differences between file structures
	def repo_difference( self ):

		# get file structures from repos
		try:
			db_list = self.db_obj.get_files_from_repo()
			ss_list = self.ss_obj.get_files_from_repo()
		except RuntimeError as re:
			raise RuntimeError( str( re ) )

		# make a list of folders from sendspace, this will allow searching available folders
		self.ss_current_folders.append( RepoFile( True, "", "0" ) ) # Sendspace root folder		
		if ss_list:
			for s in ss_list:
				if s.get_is_dir():
					self.ss_current_folders.append( s )
					#print( "sendspace "+s.get_path() + " %% " + str( s.get_file_id() ) )

		# find differences between repos
		if db_list:
			for d in db_list:
				if d not in ss_list:
					if d.get_is_dir():						
						self.dir_difference.append( d )
					else:
						self.file_difference.append( d )
					#print( d.get_path() + " %% " + str( d.get_is_dir() ) )


	# create missing folders on sendspace repository
	def create_folder_on_sendspace( self, folder ):

		"""
			We need the folder information from sendspace as it requires parent_folder_ids
			to create new folders
			Dropbox folder looks like the following;
			"/first/second/third"
			For the above folder we should look for "/first/second" in sendspace folder list
			recursively ( "/first" might exist in sendspace but "/second" might not )
		"""
		try:
			
			# gets parent folder
			pf = re.sub( "\/[^\/]*$", "", folder )
			pfid = self.get_ss_parent_id( pf )
			
			# create folder on sendspace
			ta = folder.split( "/" ) # get last level as the folder's name			
			nf = self.ss_obj.create_folder( ta[ len(ta) - 1 ], pfid, pf )
			# add newly created folder to list
			self.ss_current_folders.append( RepoFile( True, nf.get_path(), nf.get_file_id(), nf.get_parent_id() ) )
   
		except RuntimeError as e:
			self.error_list.append( "Folder '" + folder + "' could not be created on sendspace: ( Reason: " + str( e ) + ")" )


	# find parent_folder_id for given folder
	def get_ss_parent_id( self, folder ):

		for sf in self.ss_current_folders:
			if sf.get_path() == folder:
				#print( "parent folder id: " + sf.get_file_id() )
				return sf.get_file_id()
		# folder does not exist look for parent folder
		return self.create_folder_on_sendspace( folder )


	# transfer the files in the difference array
	def transfer_files( self ):

		t_list = []
		count = 0

		try:

			# loop all files to be uploaded and start threads for each of them
			for f in self.file_difference:

				# find the folder_id on sendspace for given file
				pfid = self.get_ss_parent_id( re.sub( "\/[^\/]*$", "", f.get_path() ) )
				f.set_parent_id( pfid )

				# start transfer thread
				t_list.append( FileTransfer( count, f, self.db_obj, self.ss_obj, self.local_path ) )
				count = count + 1

			# start threads
			for th in t_list:
				th.start()

			# conclude threads
			for th in t_list:
				th.join()
				if th.is_completed():
					self.error_list.append( "File uploaded successfully! {}".format( th.get_file().get_path() ) )
				else:
					self.error_list.append( "Following upload encountered an error during process: Path:{} / Message: {}"
												.format( th.get_file().get_path(), th.get_file().get_message() ) )

		except Exception as e:
			raise RuntimeError( str( e ) )
