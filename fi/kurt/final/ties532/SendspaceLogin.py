from fi.kurt.final.ties532.SendspaceCommunicator import *
from fi.kurt.final.ties532.RepoFile import *
import hashlib

# Class for handling sendspace log-in process
class SendspaceLogin:

	def __init__( self, api_key ):
		self.api_key = api_key
		self.ss_com = SendspaceCommunicator( "http://api.sendspace.com/rest/" )
		self.api_version = "1.2"

	# main method for sendspace log-in process
	def sendspace_login( self, username, password ):

		token = self.get_token();
		if token:
			# login with given user credentials
			return self.do_login( username, password, token )		
		else:
			raise RuntimeError( "Token can not be retrieved from sendspace" ) 

	# send space method call
	# ex: http://api.sendspace.com/rest/?method=files.getinfo&session_key=e7s8jvvj5n5u60qm4l6rzkax3a4ucpcg&file_id=6wnhxi
	def get_token( self ):

		params = [
					( 'api_key', self.api_key ),
				   	( 'api_version', self.api_version ) 
				 ]
		content = self.ss_com.call_ss_method( "auth.createtoken", params )

		if content:
			return content[ "params" ][ "token" ][ 0 ][ "value"]
		else:		
			raise RuntimeError( "Token can not be retrieved from sendspace" ) 

	# do login get session key
	def do_login( self, username, password, token ):

		# add given token to password, as requested by sendspace api
		password_md5 = hashlib.md5()
		password_md5.update( str.lower( str( password ) ).encode("utf-8") )
		password_md5 = password_md5.hexdigest()
		tokened_password = hashlib.md5()
		tokened_password.update( str.lower( str( token + password_md5 ) ).encode("utf-8") )
		tokened_password = tokened_password.hexdigest()

		params = [ 
					( 'user_name', username ),
				   	( 'tokened_password', tokened_password ),
				   	( 'token', token ) 
				 ]
		content = self.ss_com.call_ss_method( "auth.login", params )
		if content[ "result" ][ "status" ] == "ok":
			return content[ "params" ][ "session_key" ][ 0 ][ "value" ]
		else:
			raise RuntimeError( "Authentication to sendspace failed!" ) 

	def do_logout( self, session_key ):
		#print( "sess: " + session_key )
		params = [ ( "session_key", session_key ) ]
		try:

			content = self.ss_com.call_ss_method( "auth.logout", params )
			# error code 6 indicates, session_expired or not_exists on sendspace
			if content[ "result" ][ "status" ] == "ok" or content[ "params" ][ "error" ][ 0 ][ "attributes" ][ "code" ] == "6":
				return True
			else:
				raise RuntimeError( "Session could not be ended!" )

		except RuntimeError as re:
			raise RuntimeError( str( re ) ) 	
