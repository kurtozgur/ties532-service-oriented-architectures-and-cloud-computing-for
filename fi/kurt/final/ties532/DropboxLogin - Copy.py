from dropbox.client import DropboxOAuth2Flow

APP_KEY = "r3bdtucp6h1s7lh"
APP_SECRET = "lsu56ld0891lwg4"

# Start dropbox auth process
def dropbox_auth_start( web_app_session, request ):
    return redirect( get_dropbox_auth_flow( web_app_session ).start() )

def get_dropbox_auth_flow( web_app_session ):
    redirect_uri = "http://localhost:5000/finish-dbox"
    return DropboxOAuth2Flow( APP_KEY, APP_SECRET, redirect_uri,
                                web_app_session, "dropbox-auth-csrf-token" )

# Finish dropbox auth process
def dropbox_auth_finish( web_app_session, request ):
    try:
        access_token, user_id, url_state = \
                get_dropbox_auth_flow( web_app_session ).finish( request.args )
        web_app_session[ "dbtoken" ] = access_token
        return redirect( "/" )
    except DropboxOAuth2Flow.BadRequestException as e:
        http_status(400)
    except DropboxOAuth2Flow.BadStateException as e:
        return redirect( "/" )
    except DropboxOAuth2Flow.CsrfException as e:
        http_status(403)
    except DropboxOAuth2Flow.NotApprovedException as e:
        flash( 'Not approved!' )
        return redirect("/")
    except DropboxOAuth2Flow.ProviderException as e:
        logger.log( "Auth error: %s" % (e,) )
        http_status(403)

#log-out dropbox session, simply remove dropbox token in session
def dropbox_logout( session ):
    if "dbtoken" in session:
        del session[ "dbtoken" ]
        flash( "You've successfully logged-out of Dropbox" )
        return redirect("/")
    else:
        flash( "You don't have an active session for Dropbox" )
        return redirect("/")
