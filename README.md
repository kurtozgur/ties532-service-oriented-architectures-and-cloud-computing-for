By: �zg�r Kurt ( JYU - Web Intelligence and Service Development )
For: TIES532 - Final Project

I have used Python with Flask framework for this project. (Description of the task is in TASK_DESCRIPTION.md file)

index.py handles the requests and calls the required processes and GUI templates (index.html)
fi.kurt.final.ties532 package holds the classes used for communicating and interacting with Dropbox and sendspace APIs.

SynchController is the main class controlling the synchronisation operation. 
First, file/folder structures on both services are retrieved into lists. Folder and file differences are held in separate lists.
Missing folders are created first, and then missing files are transferred in threads. One thread handles a single file's download, upload, and deletion from local system. 

Comparing file systems at once prevents making too many queries to services. But on the other hand, if there were to be thousands of new files on Dropbox system, holding these in an array might cause problems, even though the array holds objects of RepoFile class, which does not hold too much information.

"requests" library is used to download files from dropbox and upload files to sendspace. For IO operations, a default buffer size of 512 is set.

Web app URL: http://ties532.herokuapp.com/