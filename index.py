from flask import Flask, render_template, request, session, redirect, url_for, flash
from fi.kurt.final.ties532 import Dropbox
from fi.kurt.final.ties532.DropboxLogin import *
from fi.kurt.final.ties532.SendspaceLogin import *
from fi.kurt.final.ties532.SynchController import *

app = Flask(__name__)
app.secret_key = "verysecret123key"
app.config.from_object( "config" )

db_obj = DropboxLogin( app.config[ "DROPBOX_APP_KEY" ], app.config[ "DROPBOX_APP_SECRET" ] )
ss_obj = SendspaceLogin( app.config[ "SENDSPACE_API_KEY" ] )
redirect_uri = app.config[ "DROPBOX_REDIRECT_URI" ]

@app.route( "/" )
def index():
	#del session[ "ss_sessionkey" ]
	flash( "" )
	if "dbtoken" in session:
		flash( "Dropbox status: Logged in" )
	else:
		flash( "Dropbox status: Needs Login" )
	if "ss_sessionkey" in session:
		flash( "sendspace status: Logged in" )
	else:
		flash( "sendspace status: Needs Login" )
	return render_template( "index.html" )

@app.route( "/start-dbox" )
def dropbox_auth_start():
	if "dbtoken" in session:
		flash( "You already have an active session for Dropbox!" )
		redirect( "/" )	
	return redirect( db_obj.dropbox_auth_start( session, request, redirect_uri ) )

@app.route( "/finish-dbox" )
def dbox_finish_login():
	r_link, r_msg = db_obj.dropbox_auth_finish( session, request, redirect_uri )
	if r_msg:
		flash( r_msg )
	return redirect( r_link )

@app.route( "/logout-dbox" )
def dbox_logout():
	rtn = db_obj.dropbox_logout( session )
	if rtn:
		flash( "You've successfully logged-out of Dropbox" )
		return redirect("/")	
	else:
		flash( "You don't have an active session for Dropbox" )
		return redirect("/")


@app.route( "/start-sendspace", methods=['POST'] )
def sendspace_login():
	if request.method == "POST":
		if "ss_sessionkey" in session: 
			flash( "You already have an active session for sendspace!" )
			return redirect( "/" )
		username = request.form[ "ssuser" ]
		password = request.form[ "sspass" ]
		if not username or not password:
			flash( "Username and password should both be provided!" )
			return redirect( "/" )
		else:
			try:
				token = ss_obj.sendspace_login( username, password ) 
				if token:
					flash( "Logged in to sendspace " )
					session[ "ss_sessionkey" ] = token
				else: 
					flash( "Token could not be retrieved from sendspace" ) 
				return redirect( "/" )
			except RuntimeError as e:
				flash( "An error occurred while logging in to sendspace: " + str( e ) ) 
				return redirect( "/" )

@app.route( "/logout-sendspace", methods=['POST'] )
def sendspace_logout():
	if request.method == "POST":
		if "ss_sessionkey" not in session: 
			flash( "You don't have an existing session for sendspace!" )
			return redirect( "/" )
		else:
			try:
				if ss_obj.do_logout( session[ "ss_sessionkey" ] ):
					del session[ "ss_sessionkey" ]
					flash( "You have successfully ended your session for sendspace!" )
					return redirect( "/" )	
				else: 
					flash( "There was an error while ending your sendspace session!" )
					return redirect( "/" )	
			except RuntimeError as e:
				flash( str( e ) )
				return redirect( "/" )	
			

# start synchronisation from dropbox to sendspace
@app.route( "/start_synch", methods=['POST'] )
def start_file_synch():
	if "dbtoken" in session and "ss_sessionkey" in session:  
		#start synch
		sc = SynchController( session[ "dbtoken" ], session[ "ss_sessionkey" ] )
		try:

			if sc.start_synchronisation():
				# display errors from transfer operations
				if sc.get_error_list() and len( sc.get_error_list() ):					
					for s in sc.get_error_list():
						flash( str( s ) )	
			else:
				flash( "An error occurred. Synchronisation operation could not start" )

		except RuntimeError as re:
			flash( str( re ) )
		return redirect( "/" )	
	else:
		flash( "You have to log-in to both Dropbox and sendspace!" )
		return redirect( "/" )	

if __name__ == "__main__":
	app.run(debug=True)
